<%-- 
    Document   : list
    Created on : Jan 8, 2020, 8:36:41 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form action="EmployeeController" method="GET">
            <th>Full Name  <br></th>
                <th>Birth Day <br></th>
                <th>Address  <br></th>
                <th>Position  <br></th>
                <th>Department  <br></th>
                    <c:forEach var="emp" items="${employeeList}">                      
                    <TR>                       
                        <td>${emp.fullname}</td>
                        <td>${emp.birthdate}</td>
                        <td>${emp.address}</td>
                        <td>${emp.position}</td>
                        <td>${emp.department}</td>
                    </TR>
                    </c:forEach>
        </form>
    </body>
</html>
